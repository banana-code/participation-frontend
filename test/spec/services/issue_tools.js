'use strict';

describe('Service: issueTools', function () {

  // load the service's module
  beforeEach(module('ps.services'));

  // instantiate service
  var issueTools;
  beforeEach(inject(function (_issueTools_) {
    issueTools = _issueTools_;
  }));

  it('should return information about issue phase', function () {
    var info = issueTools.getPhaseInfo('CLOSED');

    expect(info.name).toBeDefined();
    expect(info.description).toBeDefined();
    expect(info.color).toBeDefined();
    expect(info.icons).toBeDefined();
  });

  it('should return null for invalid stuff', function () {
    expect(issueTools.getPhase({})).toBeNull();
    expect(issueTools.getPhaseInfo({})).toBeNull();
  });

  it('should return null for missing stuff', function () {
    expect(issueTools.getPhase({state: 'SHENANIGANS'})).toBeNull();
    expect(issueTools.getPhaseInfo('SHENANIGANS')).toBeNull();
  });

});
