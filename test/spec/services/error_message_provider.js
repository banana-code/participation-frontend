'use strict';

describe('Service: errorMessageProvider', function () {

  // load the service's module
  beforeEach(module('ps.services'));
  beforeEach(angular.mock.module({
    'message': {
      add: function(errors) {
        this.errors = errors;
      },
    },
  }));

  // instantiate service
  var _, errMsgProvider, message;
  beforeEach(inject(function (errorMessageProvider, _message_, ___) {
    _ = ___;
    errMsgProvider = errorMessageProvider.responseError;
    message = _message_;
    message.errors = null;
  }));

  // Custom matcher
  function validMessage(msg) {
    return _.isString(msg) && _.trim(msg) !== '';
  }
  beforeEach(function() {
    this.addMatchers({
      toBeValidMessages: function () {
        var nonEmptyArray = _.isArray(this.actual) && this.actual.length > 0;
        return nonEmptyArray && _.all(this.actual, validMessage);
      }
    });
  });

  describe('when receiving correct data', function () {
    // Fixtures
    var VALID_MESSAGES, VALID_ERRORS, VALID_RESPONSE;
    beforeEach(function() {
      VALID_MESSAGES = ['foo', 'bar', 'baz'];
      VALID_ERRORS = _.map(VALID_MESSAGES, function (m) {
        return { message: m };
      });
      VALID_RESPONSE = { data: { errors: VALID_ERRORS } };
    });

    it('should pass through valid messages unchanged', function () {
      errMsgProvider(VALID_RESPONSE);
      expect(message.errors).toEqual(VALID_MESSAGES);
    });

    it('should ignore additional error data', function () {
      VALID_RESPONSE.data.errors = _.map(VALID_MESSAGES, function (m) {
        return { message: m, foo: 'bar'};
      });
      errMsgProvider(VALID_RESPONSE);
      expect(message.errors).toEqual(VALID_MESSAGES);
    });

    it('should strip malformed errors', function () {
      VALID_RESPONSE.data.errors.push({ message: '' }, {});
      errMsgProvider(VALID_RESPONSE);
      expect(message.errors).toEqual(VALID_MESSAGES);
    });
  });

  describe('when receiving malformed data', function () {
    it('should return valid message for no data', function () {
      errMsgProvider({});
      expect(message.errors).toBeValidMessages();
      expect(message.errors.length).toEqual(1);
    });

    it('should return valid message for empty data', function () {
      errMsgProvider({ data: {} });
      expect(message.errors).toBeValidMessages();
      expect(message.errors.length).toEqual(1);
    });

    it('should return valid message for empty errors', function () {
      errMsgProvider({ data: { errors: [] } });
      expect(message.errors).toBeValidMessages();
      expect(message.errors.length).toEqual(1);
    });

    it('should return valid message for non-array errors', function () {
      errMsgProvider({ data: { errors: 'Bad stuff happened!' } });
      expect(message.errors).toBeValidMessages();
      expect(message.errors.length).toEqual(1);
    });

    it('should return valid message for malformed errors', function () {
      errMsgProvider({ data: { errors: [{msg: 'Oh noes!'}, {}] } });
      expect(message.errors).toBeValidMessages();
      expect(message.errors.length).toEqual(1);
    });

    it('should return valid message for empty error messages', function () {
      errMsgProvider({ data: { errors: [{message: ''}] } });
      expect(message.errors).toBeValidMessages();
      expect(message.errors.length).toEqual(1);
    });
  });
});
