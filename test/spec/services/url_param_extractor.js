'use strict';

describe('Service: urlParamExtractor', function () {

  // load the service's module
  beforeEach(module('ps.services'));

  // instantiate service
  var TEST_TEMPLATE = '/asd/:foo/qwe/:bar';
  var urlParamExtractor, extractor;
  beforeEach(inject(function (_urlParamExtractor_) {
    urlParamExtractor = _urlParamExtractor_;
    extractor = urlParamExtractor(TEST_TEMPLATE);
  }));

  it('should extract parameters from a given URL', function () {
    var params = extractor('/asd/1/qwe/1');
    expect(params).toEqual({foo: '1', bar: '1'});
  });

  it('should handle arbitrary parameter values', function () {
    var params = extractor('/asd/ಠ_ಠ/qwe/ Mumbo Jumbo ');
    expect(params).toEqual({foo: 'ಠ_ಠ', bar: ' Mumbo Jumbo '});
  });

  it('should handle missing parameters', function () {
    var params = extractor('/asd/xxx/qwe');
    expect(params).toEqual({foo: 'xxx'});

    params = extractor('/asd/qwe/xxx');
    expect(params).toEqual({bar: 'xxx'});
  });

  it('should handle non-matching URLs', function () {
    var params = extractor('utter nonsense');
    expect(params).toEqual({});
  });

});
