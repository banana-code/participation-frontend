'use strict';

describe('Service: idsFromUrlInterceptor', function () {

  // load the service's module
  beforeEach(module('ps.services'));

  // Fixtures
  var TEST_RESPONSE;
  var TEST_TEMPLATE = '/asd/:fooId/qwe/:barId';

  // instantiate service
  var idsFromUrlInterceptor, interceptor;
  beforeEach(inject(function (_idsFromUrlInterceptor_) {
    idsFromUrlInterceptor = _idsFromUrlInterceptor_;
    TEST_RESPONSE = { config: { url: '/asd/1/qwe/2' } };
  }));

  describe('Scalar interceptor', function () {
    beforeEach(function() {
      interceptor = idsFromUrlInterceptor(TEST_TEMPLATE).response;
      TEST_RESPONSE.resource = {};
    });

    it('should attach request IDs to the response resource', function () {
      expect(interceptor(TEST_RESPONSE)).toEqual({fooId: 1, barId: 2});
    });

    it('should preserve existing IDs', function () {
      TEST_RESPONSE.resource.barId = 42;
      expect(interceptor(TEST_RESPONSE)).toEqual({fooId: 1, barId: 42});
    });
  });

  describe('Array interceptor', function () {
    beforeEach(function() {
      interceptor = idsFromUrlInterceptor(TEST_TEMPLATE, true).response;
      TEST_RESPONSE.resource = [{}, {}];
    });

    it('should attach request IDs to the response arrays', function () {
      var expected = {fooId: 1, barId: 2};
      expect(interceptor(TEST_RESPONSE)).toEqual([expected, expected]);
    });
  });
});
