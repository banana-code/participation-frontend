'use strict';

describe('Filter: ifOne', function () {

  // load the filter's module
  beforeEach(module('ps.filters'));

  // initialize a new instance of the filter before each test
  var ifOne;
  beforeEach(inject(function ($filter) {
    ifOne = $filter('ifOne');
  }));

  it('should return singular for an array with one entry', function () {
    expect(ifOne([5], 'singular', 'plural')).toEqual('singular');
  });

  it('should return plural for an array with more entries', function () {
    expect(ifOne([7, 11], 'singular', 'plural')).toEqual('plural');
  });

  it('should return plural for an empty array', function () {
    expect(ifOne([], 'singular', 'plural')).toEqual('plural');
  });

  it('should return singular for 1', function () {
    expect(ifOne(1, 'singular', 'plural')).toEqual('singular');
  });

  it('should return plural for all numbers except 1', function () {
    expect(ifOne(13, 'singular', 'plural')).toEqual('plural');
    expect(ifOne(0, 'singular', 'plural')).toEqual('plural');
  });

  it('should return plural for null', function () {
    expect(ifOne(null, 'singular', 'plural')).toEqual('plural');
  });

});
