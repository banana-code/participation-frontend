'use strict';

describe('Filter: count', function () {

  // load the filter's module
  beforeEach(module('ps.filters'));

  // initialize a new instance of the filter before each test
  var count;
  beforeEach(inject(function ($filter) {
    count = $filter('count');
  }));

  it('should return the size of an array', function () {
    expect(count([])).toBe(0);
    expect(count([42])).toBe(1);
  });

  it('should regard null as empty', function () {
    expect(count(null)).toBe(0);
  });

  it('should return the second argument for empty arrays if present"', function () {
    expect(count([], 0)).toBe(0);
    expect(count([], '')).toBe('');
    expect(count([], 'empty')).toBe('empty');
  });

});
