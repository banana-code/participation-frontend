# Markdown

### Was ist Markdown?
Markdown ist eine vereinfachte Auszeichnungssprache.

### Warum Markdown?
Ein Ziel von Markdown ist, dass schon die Ausgangsform ohne weitere Konvertierung leicht lesbar ist. Als Auszeichnungselemente wurden daher vor allem Auszeichnungsarten verwendet, die in Plaintext und E-Mails üblich sind.

### Wie benutze ich es?


#### Einfacher Text
Einfacher Text bleibt einfacher Text.


#### Überschriften
```
# Überschrift in Ebene 1
## Überschrift in Ebene 2
### Überschrift in Ebene 3
#### Überschrift in Ebene 4
##### Überschrift in Ebene 5
###### Überschrift in Ebene 6
```
<blockquote>
# Überschrift in Ebene 1
## Überschrift in Ebene 2
### Überschrift in Ebene 3
#### Überschrift in Ebene 4
##### Überschrift in Ebene 5
###### Überschrift in Ebene 6
</blockquote>


#### Fett und Kursiv
```
*Kursiv*, **Fett** und ***Fett kursiv***
```
*Kursiv*, **Fett** und ***Fett kursiv***


#### Durchgestrichen
```markdown
~~Falscher text.~~
```
~~Falscher text.~~

#### Hyperlinks
```
[Beschriftung des Links](http://goo.gl/CTkqcN)
```
[Beschriftung des Links](http://goo.gl/CTkqcN)


#### Bilder
Bilder können von beliebigen Hostern eingefügt werden.  
Beispielsweise können auf [directupload.net](http://www.directupload.net/) Bilder hochgeladen werden.
Den Link den man unter "5.) Hotlink" erhält, kann man dann wie folgt einbinden:

```
![Alternativtext](http://s14.directupload.net/images/141007/uwwyngwg.jpg)
```
![Alternativtext](http://s14.directupload.net/images/141007/uwwyngwg.jpg)
