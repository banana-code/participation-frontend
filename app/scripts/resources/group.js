'use strict';

angular.module('ps.resources')
  .factory('Group', function (psResource, _, session, Member) {

    // TODO remove after backend does it right ;)
    function transformMembership (group) {
      var member = new Member({
        id: group.userMembershipId !== -1 ? group.userMembershipId : null,
        groupId: group.id,
        user: session.user(),
        state: group.userState,
      });
      return _(group)
        .omit('userMembershipId', 'userState')
        .assign({userMembership: member})
        .value();
    }

    var Group = psResource(psResource.paths.group, {
      groupId: '@id'
    }, function (actions) {
      ['get', 'save', 'patch'].forEach(function (method) {
        actions[method].transformResponse.push(transformMembership);
      });
      actions.query.transformResponse
        .push(_.partial(_.map, _, transformMembership));
      actions.patch.transformRequest
        .unshift(psResource.whitelister(['title', 'description']));
      return actions;
    });

    Group.prototype.getBlankMembership = function (user) {
      return new Member({
        groupId: this.id,
        user: user,
      });
    };

    Group.prototype.getMembers = function (success, error) {
      return Member.query({groupId: this.id}, success, error);
    };

    return Group;
  });
