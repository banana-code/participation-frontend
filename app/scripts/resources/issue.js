'use strict';

angular.module('ps.resources')
  .factory('Issue', function (psResource, _, issueTools) {

    var requestFilter = psResource.whitelister([
      'title', 'description', 'startDate', 'solutionEndDate', 'voteEndDate',
    ]);

    function addPhaseInfo (issue) {
      issue.phase = issueTools.getPhase(issue);
      return issue;
    }

    return psResource(psResource.paths.issue, {
      issueId: '@id',
      groupId: '@groupId',
    }, function (actions) {
      actions.save.transformRequest.unshift(requestFilter);
      actions.patch.transformRequest.unshift(requestFilter);
      actions.get.transformResponse.push(addPhaseInfo);
      actions.query.transformResponse.push(_.partial(_.map, _, addPhaseInfo));
      return actions;
    });
  });
