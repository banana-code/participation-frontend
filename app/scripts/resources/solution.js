'use strict';

angular.module('ps.resources')
  .factory('Solution', function (psResource) {

    return psResource(psResource.paths.solution, {
      solutionId: '@id',
      issueId: '@issueId',
      groupId: '@groupId',
    }, function (actions) {
      actions.save.transformRequest.unshift(
        psResource.whitelister(['title', 'description'])
      );
      return actions;
    });
  });
