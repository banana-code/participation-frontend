'use strict';

angular.module('ps.resources')
  .factory('Member', function (psResource, _, membershipTools) {

    var Member =  psResource(psResource.paths.member, {
      memberId: '@id',
      groupId: '@groupId',
    }, function (actions) {
      actions.save.transformRequest.unshift(function (member) {
        return {
          userId: member.user.id,
        };
      });
      // TODO remove this after backend has been normalized
      actions.patch.transformRequest.unshift(function (member) {
        return {
          userId: member.userId,
          membershipState: member.state,
        };
      });
      return actions;
    });

    // Make static mt methods available on Member instances
    var mt = membershipTools;
    _(mt).omit('localStateName', 'equals').each(function (method, name) {
      Member.prototype[name] = function (object) {
        return method(this, object);
      };
    }).commit();

    Member.prototype.sameState = function (object) {
      return mt.equals(this, object);
    };

    Member.prototype.sameUser = function (otherMember) {
      return otherMember.user.id === this.user.id;
    };

    return Member;
  });
