'use strict';

angular
  .module('ps.resources', [
    'ps.services',
    'ngResource',
  ])
  .factory('psResource', function ($resource, _, config, util, idsFromUrlInterceptor) {

    var paths = {};
    paths.group = config.apiUrl + '/groups/:groupId';
    paths.member = paths.group + '/members/:memberId';
    paths.issue = paths.group + '/issues/:issueId';
    paths.solution = paths.issue + '/solutions/:solutionId';

    function defaultActionsFor(url) {
      function addDefaults (o) {
        return _.defaults(o, {
          transformRequest: [angular.toJson],
          transformResponse: [angular.fromJson, util.parseDates],
          // TODO remove this when we get the IDs from the backend
          interceptor: idsFromUrlInterceptor(url, o.isArray),
        });
      }
      return {
        save: addDefaults({ method: 'POST' }),
        patch: addDefaults({ method: 'PATCH' }),
        get: addDefaults({ method: 'GET' }),
        query: addDefaults({ method: 'GET', isArray: true }),
      };
    }

    var psResource = function (url, paramDefaults, actionsMutator, options) {
      var actions = (actionsMutator || _.identity)(defaultActionsFor(url));
      return $resource(url, paramDefaults, actions, options);
    };

    psResource.paths = paths;
    psResource.whitelister = function (keys) {
      return _.partial(_.pick, _, keys);
    };

    return psResource;
  });
