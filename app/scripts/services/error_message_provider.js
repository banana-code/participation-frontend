'use strict';

angular.module('ps.services')
  .factory('errorMessageProvider', function errorMessageProvider($q, _, message) {

    var code2msg = {
      401: 'http_unauthorized',
      403: 'http_forbidden',
    };

    function parseErrors(data) {
      var errors = _.get(data, 'errors', []);
      if (!_.isArray(errors)) {
        return [];
      }
      return _.filter(_.pluck(errors, 'message'));
    }

    return {
      responseError: function(response) {
        var errors = parseErrors(response.data);
        if (errors.length === 0) {
          errors.push(_.get(code2msg, response.status, 'general_error'));
        }
        message.add(errors);
        return $q.reject(response);
      }
    };
  });
