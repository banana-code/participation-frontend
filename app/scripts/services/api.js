'use strict';

/**
 * @ngdoc service
 * @name participationApp.api
 * @description
 * # api
 * Factory in the participationApp.
 */
angular.module('ps.services')
  .factory('api', function ($http, config, session, _) {

    var apiUrl = config.apiUrl;

    function response(data, callback) {
      if (typeof callback === 'function') {
        callback(data);
      }
    }

    function normalizeUser(userData) {
      var user = userData.user;
      user.email = userData.email;
      user.state = userData.state;
      user.isAdmin = function() {
        return user.state === 'ADMIN';
      };
      return user;
    }

    function setToken(token) {
      $http.defaults.headers.common['X-Token'] = token;
    }

    function setSessionFromResponse(sessionData) {
      setToken(sessionData.rememberMeToken);
      session.setToken(sessionData.rememberMeToken);
      session.setUser(normalizeUser(sessionData));
    }

    return {
      setSessionFromResponse: setSessionFromResponse,
      restoreSession: function (callback) {
        if (session.loggedIn()) {
          setToken(session.getToken());
          this.get('/me', function (userData) {
            session.setUser(normalizeUser(userData));
            callback(true);
          }, function () {
            session.invalidate();
            callback(false);
          });
        }
      },
      confirm: function (key, callback, errorCallback) {
        var requestData = {key: key};
        this.post('/confirm', requestData, function (data) {
          setSessionFromResponse(data);
          response('confirmation_success', callback);
        }, errorCallback);
      },
      login: function (email, password, callback, errorCallback) {
        var requestData = {
          email: email,
          password: password,
        };
        this.post('/login', requestData, function (data) {
          setSessionFromResponse(data);
          response('login_success', callback);
        }, errorCallback);
      },
      logout: function (callback, errorCallback) {
        this.post('/logout', {}, function () {
          setToken(null);
          session.invalidate();
          response('logout_success', callback);
        }, errorCallback);
      },
      get: function (path, callback, errorCallback, params) {
        var opt = {
          url: apiUrl + path,
          method: 'GET',
          params: params,
        };
        return $http(opt)
          .success(callback || _.noop)
          .error(errorCallback ||  _.noop);
      },
      put: function (path, data, callback, errorCallback) {
        return $http.put(apiUrl + path, JSON.stringify(data))
          .success(callback || _.noop)
          .error(errorCallback ||  _.noop);
      },
      post: function (path, data, callback, errorCallback) {
        return $http.post(apiUrl + path, JSON.stringify(data))
          .success(callback || _.noop)
          .error(errorCallback ||  _.noop);
      },
      patch: function (path, data, callback, errorCallback) {
        var opt = {
          url: apiUrl + path,
          data: data,
          method: 'PATCH',
        };
        return $http(opt)
          .success(callback || _.noop)
          .error(errorCallback ||  _.noop);
      },
      del: function (path, callback, errorCallback) {
        var opt = {
          url: apiUrl + path,
          method: 'DELETE',
        };
        return $http(opt)
          .success(callback || _.noop)
          .error(errorCallback ||  _.noop);
      }
    };
  });
