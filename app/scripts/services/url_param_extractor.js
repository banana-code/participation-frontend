'use strict';

angular.module('ps.services')
  .factory('urlParamExtractor', function () {

    return function urlParamExtractor (template) {
      var keys = [];
      var urlMatcher = new RegExp(template.replace(/(\/)?:(\w+)/g, function(_, slash, key) {
        // jshint unused: true
        keys.push(key);
        return '(?:' + (slash || '') + '([^\\/]+))?';
      }));

      return function (url) {
        var params = {};
        var values = urlMatcher.exec(url) || [];
        keys.forEach(function (key, i) {
          params[key] = values[i + 1];
        });
        return params;
      };
    };
  });
