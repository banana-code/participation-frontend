'use strict';

/**
 * @ngdoc service
 * @name participationApp.message
 * @description
 * # message
 * Factory in the participationApp.
 */
angular.module('ps.services')
  .factory('message', function ($timeout, messages) {

    var messageStore = [];

    var self = {
      all: function () {
        return messageStore;
      },
      add: function (msgs) {
        return angular.isArray(msgs) ? addAll(msgs) : add(msgs);
      },
      remove: function (msg) {
        // O(n) :(
        var i = messageStore.indexOf(msg);
        if (i > -1) {
          messageStore.splice(i, 1);
        }
      },
    };

    var addAll = function (msgs, ttl) {
      return msgs.map(function (msg) {
        return add(msg, ttl);
      });
    };

    var add = function (msg, ttl) {
      if (typeof messages[msg] === 'undefined') {
        messages[msg] = {
          'type': 'info',
          'text': msg
        };
      }
      // copy element so we can remove by reference
      var element = angular.copy(messages[msg]);
      messageStore.push(element);
      scheduleRemove(element, ttl);
      return element;
    };

    var scheduleRemove = function (element, ttl) {
      ttl = angular.isDefined(ttl) ? ttl : defaultTTL(element);
      // No timeout for falsy stuff (false, 0, '', ...)
      if (ttl) {
        $timeout(function () {
          self.remove(element);
        }, ttl);
      }
    };

    var defaultTTL = function (element) {
      return element.type === 'danger' ? false : 7000;
    };

    return self;
  });
