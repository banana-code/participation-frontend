'use strict';

angular.module('ps.services')
  .factory('psMarkdownPreview', function ($modal) {
    return function (markdown) {
      return $modal.open({
        templateUrl: '/views/ps_markdown_preview.html',
        controller: function ($scope) {
          $scope.markdown = markdown;
        },
      });
    };
  });
