'use strict';

angular.module('participationApp')
  .factory('psConfirm', function ($modal) {
    return function (question) {
      return $modal.open({
        templateUrl: '/views/ps_confirm.html',
        controller: function ($scope) {
          $scope.question = question;
        },
      }).result;
    };
  });
