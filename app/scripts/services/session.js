'use strict';

/**
 * @ngdoc service
 * @name participationApp.session
 * @description
 * # session
 * Factory in the participationApp.
 */
angular.module('ps.services')
  .factory('session', function ($window) {
    var storage = $window.localStorage;
    var user = null;
    var tokenStorageKey = 'token';

    return {
      loggedIn: function () {
        return !!storage.getItem(tokenStorageKey);
      },
      invalidate: function () {
        user = null;
        storage.removeItem(tokenStorageKey);
      },
      user: function () {
        return user;
      },
      setUser: function (userDetails) {
        user = userDetails;
      },
      getToken: function () {
        return storage.getItem(tokenStorageKey);
      },
      setToken: function (token) {
        storage.setItem(tokenStorageKey, token);
      },
    };
  });
