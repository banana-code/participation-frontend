'use strict';

angular.module('ps.services')
  .factory('idsFromUrlInterceptor', function (_, urlParamExtractor) {
    function missingIdAdderFor(urlTemplate) {
      var paramExtractor = urlParamExtractor(urlTemplate);
      return function (url) {
        var ids = _(paramExtractor(url))
          .omit(_.isUndefined)
          .mapValues(_.partial(parseInt, _, 10))
          .value();
        return _(_.defaults).partial(_, ids).ary(1).value();
      };
    }

    var responseInterceptors = {
      scalar: function(missingIdAdder) {
        return function (response) {
          return missingIdAdder(response.config.url)(response.resource);
        };
      },
      array: function(missingIdAdder) {
        return function (response) {
          return response.resource.map(missingIdAdder(response.config.url));
        };
      }
    };

    return function (urlTemplate, responseIsArray) {
      var missingIdAdder = missingIdAdderFor(urlTemplate);
      var interceptorType = responseIsArray ? 'array' : 'scalar';
      return {
        response: responseInterceptors[interceptorType](missingIdAdder),
      };
    };
  });
