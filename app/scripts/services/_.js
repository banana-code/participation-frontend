'use strict';

angular.module('ps.services')
  .factory('_', function ($window) {
    return $window._;
  });
