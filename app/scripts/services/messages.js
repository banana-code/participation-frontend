'use strict';

/**
 * @ngdoc service
 * @name participationApp.messages
 * @description
 * # messages
 * Constant in the participationApp.
 */
angular.module('ps.services')
  .constant('messages', {
    'http_unauthorized': {
      'type': 'danger',
      'text': 'Du musst eingeloggt sein um diese Aktion durchzuführen.'
    },
    'http_forbidden': {
      'type': 'danger',
      'text': 'Du bist nicht berechtigt diese Aktion durchzuführen.'
    },

    'not_implemented_yet': {
      'type': 'warning',
      'text': 'Dieses Feature wurde noch nicht implementiert.'
    },
    'general_error': {
      'type': 'danger',
      'text': 'Ups, da ist wohl irgendwas schief gelaufen…'
    },
    'login_failed': {
      'type': 'danger',
      'text': 'Login fehlgeschlagen.'
    },

    'register_email_taken': {
      'type': 'danger',
      'text': 'Mail bereits registriert.'
    },
    'register_email_invalid': {
      'type': 'danger',
      'text': 'Im Moment werden nur E-Mail Adressen der Domain \'kit.edu\' akzeptiert.'
    },
    'register_success': {
      'type': 'success',
      'text': 'Du hast dich erfolgreich registriert.'
    },
    'register_failed': {
      'type': 'danger',
      'text': 'Registrierung fehlgeschlagen.'
    },

    'password_short': {
      'type': 'danger',
      'text': 'Passwort ist zu kurz.'
    },
    'password_invalid': {
      'type': 'danger',
      'text': 'Das angegebene Passwort ist ungültig.'
    },
    'password_insecure': {
      'type': 'danger',
      'text': 'Das angegebene Passwort ist zu unsicher.'
    },
    'password_change_incorrect': {
      'type': 'danger',
      'text': 'Das aktuelle Passwort ist nicht korrekt.'
    },
    'password_change_same': {
      'type': 'warning',
      'text': 'Neues und altes Passwort sind identisch.'
    },
    'password_change_success': {
      'type': 'success',
      'text': 'Du hast dein Passwort erfolgreich geändert.'
    },

    'alias_taken': {
      'type': 'danger',
      'text': 'Das Pseudonym ist bereits vergeben.'
    },
    'alias_empty': {
      'type': 'danger',
      'text': 'Das Pseudonym darf nicht leer sein.'
    },
    'alias_invalid': {
      'type': 'danger',
      'text': 'Das Pseudonym ist ungültig.'
    },
    'alias_toolong': {
      'type': 'danger',
      'text': 'Das Pseudonym ist zu lang.'
    },
    'alias_change_failed': {
      'type': 'danger',
      'text': 'Das Ändern deines Pseudonyms ist fehlgeschlagen.'
    },
    'alias_change_success': {
      'type': 'success',
      'text': 'Dein Pseudonym wurde erfolgreich geändert.'
    },

    'issue_title_invalid': {
      'type': 'danger',
      'text': 'Bitte gib einen Titel für das Thema an.'
    },
    'issue_title_too_long': {
      'type': 'danger',
      'text': 'Der Titel deines Themas ist zu lang.'
    },
    'issue_question_invalid': {
      'type': 'danger',
      'text': 'Bitte gib eine übergeordnete Fragestellung an.'
    },
    'issue_current_solution_invalid': {
      'type': 'danger',
      'text': 'Bitte gib die aktuelle Lösung an.'
    },
    'issue_solution_date_invalid': {
      'type': 'danger',
      'text': 'Das angegebene Ende der Vorschlagsphase ist ungültig.'
    },
    'issue_vote_date_invalid': {
      'type': 'danger',
      'text': 'Das angegebene Ende der Entscheidungsphase ist ungültig.'
    },
    'issue_save_success': {
      'type': 'success',
      'text': 'Das Thema wurde erfolgreich gespeichert. Es ist aber noch nicht veröffentlicht.'
    },
    'issue_publication_success': {
      'type': 'success',
      'text': 'Das Thema wurde erfolgreich veröffentlicht.'
    },
    'issue_creation_aborted': {
      'type': 'warning',
      'text': 'Du hast die Erstellung deines Themas abgebrochen.'
    },
    'issue_edit_aborted': {
      'type': 'warning',
      'text': 'Du hast die Bearbeitung deines Themas abgebrochen.'
    },
    'issue_creation_failed': {
      'type': 'danger',
      'text': 'Fehler beim Erstellen deines Themas.'
    },
    'issue_edit_started': {
      'type': 'danger',
      'text': 'Du kannst kein Thema bearbeiten, das bereits gestartet ist.'
    },

    'solution_title_empty': {
      'type': 'danger',
      'text': 'Du hast keinen Titel angegeben.'
    },
    'solution_creation_success': {
      'type': 'success',
      'text': 'Dein Lösungsvorschlag wurde erfolgreich erstellt.'
    },
    'solution_creation_aborted': {
      'type': 'warning',
      'text': 'Du hast die Erstellung deines Lösungsvorschlags abgebrochen.'
    },
    'solution_creation_failed': {
      'type': 'danger',
      'text': 'Fehler beim Erstellen deiner Lösung.'
    },
    'solution_deleted': {
      'type': 'success',
      'text': 'Der Lösungsvorschlag wurde erfolgreich gelöscht.'
    },

    'resistance_creation_failed': {
      'type': 'danger',
      'text': 'Widerstand konnte nicht angelegt werden.'
    },
    'resistance_invalid_number': {
      'type': 'danger',
      'text': 'Keine Widerstände größer 10 oder kleiner 0 möglich.'
    },
    'resistance_update_failed': {
      'type': 'danger',
      'text': 'Widerstand konnte nicht aktualisiert werden.'
    },

    'group_title_invalid': {
      'type': 'danger',
      'text': 'Bitte gib einen Titel der Gruppe ein, die du erstellen willst.'
    },
    'group_title_toolong' : {
      'type': 'danger',
      'text': 'Dein Titel ist zu lang. Bitte wähle einen kürzeren.'
    },
    'group_title_taken': {
      'type': 'danger',
      'text': 'Es existiert bereits eine Gruppe mit diesem Titel.'
    },
    'group_creation_success': {
      'type': 'success',
      'text': 'Die Gruppe wurde erfolgreich erstellt.'
    },
    'group_save_success': {
      'type': 'success',
      'text': 'Die Gruppe wurde erfolgreich geändert.'
    },
    'group_creation_aborted': {
      'type': 'warning',
      'text': 'Das Erstellen der Gruppe wurde abgebrochen.'
    },
    'group_edit_aborted': {
      'type': 'warning',
      'text': 'Die Änderungen wurden verworfen.'
    },
    'logout_success': {
      'type': 'success',
      'text': 'Du wurdest erfolgreich ausgeloggt.'
    },
    'logout_fail': {
      'type': 'danger',
      'text': 'Es gab ein Problem beim Logout.'
    },
    'token_expired': {
      'type': 'warning',
      'text': 'Dein Token ist abgelaufen, bitte melde dich erneut an.'
    },
    'invite_user_requested': {
      'type': 'success',
      'text': '<p>Der Nutzer hat bereits eine Mitgliedsanfrage gestellt.</p><p>Der Nutzer ist nun Mitglied.</p>'
    },
    'invite_already_member': {
      'type': 'info',
      'text': 'Der Nutzer ist bereits Mitglied dieser Gruppe.'
    },
    'invite_already_invited': {
      'type': 'info',
      'text': 'Der Nutzer ist bereits eingeladen.'
    },
    'invite_success': {
      'type': 'success',
      'text': 'Der Benutzer wurde eingeladen.'
    },
    'issue_deadline_order': {
      'type': 'danger',
      'text': 'Die Vorschlagsphase darf nicht nach der Entscheidungsphase enden.'
    },
    'issue_solution_date_past': {
      'type': 'danger',
      'text': 'Das angegebene Ende der Vorschlagsphase ist nicht in der Zukunft.'
    },
    'issue_vote_date_past': {
      'type': 'danger',
      'text': 'Das angegebene Ende der Entscheidungsphase ist nicht in der Zukunft.'
    },
    'issue_deleted': {
      'type': 'success',
      'text': 'Das Thema wurde erfolgreich gelöscht.'
    },
    'group_leave_success': {
      'type': 'success',
      'text': 'Du bist aus der Gruppe ausgetreten.'
    },
    'resistance_change_failed': {
      'type': 'danger',
      'text': 'Dein Widerstand konnte nicht gespeichert werden.'
    },
    'group_deleted': {
      'type': 'success',
      'text': 'Die Gruppe wurde gelöscht.'
    },
    'login_password_wrong': {
      'type': 'danger',
      'text': 'Das angegebene Passwort ist leider falsch.'
    },
    'login_email_wrong': {
      'type': 'danger',
      'text': 'Die angegebene E-Mail Adresse ist uns leider nicht bekannt.'
    },
    'membership_too_few_owner': {
      'type': 'danger',
      'text': '<p>Du bist der letzte Gruppeneigner.</p>' +
        '<p>Um diese Aktion durchzuführen musst du mindestens ein anderes Mitglied zum Gruppeneigner ernennen.</p>'
    },
    'group_left': {
      'type': 'success',
      'text': 'Du hast die Gruppe verlassen.'
    },
    'solution_deletion_zero_option': {
      'type': 'danger',
      'text': 'Du kannst die Nulllösung nicht löschen.'
    },
    'comment_empty': {
      'type': 'warning',
      'text': 'Du kannst kein Kommentar ohne Text abschicken.'
    },
    'comment_no_type': {
      'type': 'warning',
      'text': 'Bitte gib den Typ deines Kommentars an.'
    },
    'confirmation_success': {
      'type': 'success',
      'text': 'Dein Account wurde erfolgreich aktiviert.'
    },
    'already_confirmed_or_invalid_key': {
      'type': 'danger',
      'text': 'Der von dir angegebene Bestätigungscode ist leider nicht gültig.'
    },
    'email_invalid': {
      'type': 'danger',
      'text': 'Der von dir angegebene E-Mail-Adresse ist uns leider nicht bekannt.'
    },
  });
