'use strict';

/**
 * @ngdoc service
 * @name participationApp.issueTools
 * @description
 * # issueTools
 * Service in the participationApp.
 */
angular.module('ps.services')
  .factory('issueTools', function issueTools() {

    var phaseInfo = {
      'UNPUBLISHED': {
        name: 'unpublished',
        description: 'Das Thema ist noch unveröffentlicht',
        color: 'muted',
        icons: [
          'fa fa-inbox',
        ],
      },
      'OPEN': {
        name: 'solution',
        description: 'Abstimmung und Einbringung von Lösungsvorschlägen sind möglich',
        color: 'primary',
        icons: [
          'envelope',
          'pencil-square-o',
        ],
      },
      'VOTE_ONLY': {
        name: 'vote',
        description: 'Abstimmung ist möglich',
        color: 'primary',
        icons: [
          'pencil-square-o',
        ],
      },
      'CLOSED': {
        name: 'finished',
        description: 'Dieses Thema ist abgeschlossen',
        color: 'danger',
        icons: [
          'flag-checkered',
        ],
      },
    };

    var getPhaseInfo = function (phase) {
      return phaseInfo[phase] || null;
    };

    return {
      getPhase: function (issue) {
        return !angular.isObject(issue) ? null : getPhaseInfo(issue.state);
      },
      getPhaseInfo: getPhaseInfo
    };

  });
