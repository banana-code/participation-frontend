'use strict';

/**
 * @ngdoc service
 * @name participationApp.membershipTools
 * @description
 * # membershipTools
 * Service in the participationApp.
 */
angular.module('ps.services')
  .factory('membershipTools', function () {

    var membershipGerman = {
      'OWNER': 'Gruppeneigner',
      'MODERATOR': 'Moderator',
      'MEMBER': 'Mitglied',
      'INVITED': 'Eingeladen',
      'REQUESTED': 'Angefragt',
    };

    // Higher value means more privileges, only positive values
    var stateRanking = {
      'REQUESTED':  10,
      'INVITED':    20,
      'MEMBER':     30,
      'MODERATOR':  40,
      'OWNER':      50,
    };

    // Handle membership objects as well as state strings
    var ensureState = function (object) {
      return object && object.hasOwnProperty('state') ? object.state : object;
    };

    var getRanking = function (state) {
      return stateRanking[ensureState(state)] || -1;
    };

    var atLeast = function (subject, object) {
      return getRanking(subject) >= getRanking(object);
    };

    var equals = function (subject, object) {
      return getRanking(subject) === getRanking(object);
    };

    var service = {
      getRanking: getRanking,
      equals: equals,
      atLeast: atLeast,

      isUnknown: function (subject) {
        return !subject || ensureState(subject) === null;
      },

      // TODO do this properly...and somewhere else
      localStateName: function (state) {
        return membershipGerman[state];
      },
    };

    // Add checker functions for each state
    // 'MEMBER' -> isMember and atLeastMember
    var capitalize = function (string) {
      return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    };
    Object.keys(stateRanking).forEach(function (state) {
      var capitalized = capitalize(state);

      service['is' + capitalized] = function (subject) {
        return equals(subject, state);
      };
      service['atLeast' + capitalized] = function (subject) {
        return atLeast(subject, state);
      };
    });

    return service;
  });
