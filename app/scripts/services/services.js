'use strict';

angular
  .module('ps.services', [
    'angularMoment',
    'focusOn',
  ]);
