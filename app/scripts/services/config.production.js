'use strict';

angular.module('ps.services')
  .constant('config', {
    apiUrl: 'https://service-participation.rhcloud.com/api',
  });
