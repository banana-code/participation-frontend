'use strict';

/**
 * @ngdoc service
 * @name participationApp.config
 * @description
 * # config
 * Constant in the participationApp.
 */
angular.module('ps.services')
  .constant('config', {

    // Use API from the same server serving the frontend
    // apiUrl: '/api',

    // Standard location of development server
    apiUrl: 'http://localhost:8080/api',

    // Activate this to display debug information around the app
    debug: false,

  });
