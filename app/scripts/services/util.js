'use strict';

/**
 * @ngdoc service
 * @name participationApp.util
 * @description
 * # util
 * Factory in the participationApp.
 */
angular.module('ps.services')
  .factory('util', function ($timeout, focus, _) {

    // TODO move to interceptor
    var dateKeys = [
      'registrationDate',
      'createdDate',
      'lastModifiedDate',
      'startDate',
      'solutionEndDate',
      'voteEndDate',
      'publishedDate',
    ];
    function isDateKey(key) {
      return dateKeys.indexOf(key) > -1;
    }
    function isDateValue(val) {
      return _.isString(val) || _.isNumber(val);
    }
    function parseDatesInternal(obj) {
      if (!angular.isObject(obj)) {
        return;
      }
      angular.forEach(obj, function (value, key) {
        parseDatesInternal(value);
        if (isDateValue(value) && isDateKey(key)) {
          obj[key] = new Date(value);
        }
      });
    }

    return {
      parseDates: function (obj) {
        parseDatesInternal(obj);
        return obj;
      },

      setFocus: function (elementName) {
        $timeout(function () {
          focus(elementName);
        }, 200);
      }
    };
  });
