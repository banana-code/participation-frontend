'use strict';

/**
 * @ngdoc overview
 * @name participationApp
 * @description
 * # participationApp
 *
 * Main module of the application.
 */
angular
  .module('participationApp', [
    'ps.services',
    'ps.filters',
    'ps.resources',
    'ps.controllers',
    'ngRoute',
    'crumble',
    'btford.markdown',
  ])
  .config(function (markdownConverterProvider) {
    // options to be passed to Showdown
    // see: https://github.com/coreyti/showdown#extensions
    markdownConverterProvider.config({
      extensions: ['github']
    });
  })
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push('errorMessageProvider');
  })
  .run(function ($rootScope, $location, config, api, message, amMoment, crumble) {
    amMoment.changeLocale('de');

    $rootScope.debug = config.debug;

    api.restoreSession(function (success) {
      if (!success) {
        message.add('token_expired');
        $location.path('/');
      }
    });

    // Configure crumble so that we can override standard parents by adding a parent property to a route
    var getParent = crumble.getParent;
    crumble.getParent = function (path) {
      var route = crumble.getRoute(path);
      return route && angular.isDefined(route.parent) ? route.parent : getParent(path);
    };
  })
  .config(function ($routeProvider) {

    function currentGroup ($route, Group) {
      return Group.get({groupId: $route.current.params.groupId}).$promise;
    }
    function currentIssue ($route, Issue) {
      return Issue.get({
        groupId: $route.current.params.groupId,
        issueId: $route.current.params.issueId,
      }).$promise;
    }

    $routeProvider

      // General routes
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        label: 'Home',
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        label: 'Registrieren',
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl',
        label: 'Profil bearbeiten',
      })

      // Group routes
      .when('/groups', {
        templateUrl: 'views/groups/list.html',
        controller: 'GroupsListCtrl',
        label: 'Gruppen',
        reloadOnSearch: false,
        resolve: {
          groups: function (Group) {
            return Group.query().$promise;
          }
        }
      })
      .when('/groups/new', {
        templateUrl: 'views/groups/edit.html',
        controller: 'GroupsEditCtrl',
        label: 'Neue Gruppe',
        resolve: {
          group: function (Group) {
            return new Group();
          }
        }
      })
      // TODO change to /groups/:groupId
      .when('/group/:groupId', {
        templateUrl: 'views/groups/view.html',
        controller: 'GroupsViewCtrl',
        label: '{{group.title}}',
        parent: '/groups',
        reloadOnSearch: false,
        resolve: { group: currentGroup }
      })
      // TODO change to /groups/:groupId/edit
      .when('/group/:groupId/edit', {
        templateUrl: 'views/groups/edit.html',
        controller: 'GroupsEditCtrl',
        label: 'Bearbeiten',
        resolve: { group: currentGroup }
      })

      // Member routes
      // TODO change to /groups/:groupId/members
      .when('/group/:groupId/members', {
        templateUrl: 'views/members/list.html',
        controller: 'MembersListCtrl',
        label: 'Mitglieder',
        reloadOnSearch: false,
        resolve: { group: currentGroup }
      })

      // Issue routes
      // TODO change to /groups/:groupId/issues/new
      .when('/group/:groupId/newissue', {
        templateUrl: 'views/issues/edit.html',
        controller: 'IssuesEditCtrl',
        label: 'Neues Thema erstellen',
        resolve: {
          group: currentGroup,
          issue: function ($route, Issue) {
            return new Issue({groupId: $route.current.params.groupId});
          },
        }
      })
      // TODO change to /groups/:groupId/issues/:issueId
      .when('/group/:groupId/issue/:issueId', {
        templateUrl: 'views/issues/view.html',
        controller: 'IssuesViewCtrl',
        label: '{{issue.title}}',
        parent: '/group/{{group.id}}',
        reloadOnSearch: false,
        resolve: {
          group: currentGroup,
          issue: currentIssue,
        }
      })
      // TODO change to /groups/:groupId/issues/:issueId/edit
      .when('/group/:groupId/issue/:issueId/edit', {
        templateUrl: 'views/issues/edit.html',
        controller: 'IssuesEditCtrl',
        label: 'Bearbeiten',
        resolve: {
          group: currentGroup,
          issue: currentIssue,
        }
      })

      // TODO change to /groups/:groupId/issues/:issueId/solutions/new
      .when('/group/:groupId/issue/:issueId/newsolution', {
        templateUrl: 'views/solutions/new.html',
        controller: 'SolutionsNewCtrl',
        label: 'Neuer Lösungsvorschlag',
        resolve: { issue: currentIssue }
      })

      .when('/activation', {
        templateUrl: 'views/activation.html',
        controller: 'ActivationCtrl',
        label: 'Account Aktivierung',
      })

      .when('/forgotPassword', {
        templateUrl: 'views/forgotPassword.html',
        controller: 'ForgotPasswordCtrl',
        label: 'Passwort vergessen',
      })

      .when('/resetPassword', {
        templateUrl: 'views/resetPassword.html',
        controller: 'ResetPasswordCtrl',
        label: 'Passwort zurücksetzen',
      })

      .when('/help', {
        templateUrl: 'views/help.html',
        controller: 'HelpCtrl',
        label: 'Hilfe',
      })

      .when('/help/:topic', {
        templateUrl: 'views/help.html',
        controller: 'HelpCtrl',
        label: '{{topic}}',
      })

      .when('/members', {
        templateUrl: 'views/members.html',
        controller: 'MembersCtrl',
        label: 'Mitgliederverwaltung',
      })

      .otherwise({
        redirectTo: '/'
      });
  });
