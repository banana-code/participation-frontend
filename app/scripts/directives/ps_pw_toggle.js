'use strict';

/**
 * @ngdoc directive
 * @name participationApp.directive:psPwInput
 * @description
 * # psPwInput
 */
angular.module('ps.directives')
  .directive('psPwToggle', function () {
    return {
      restrict: 'E',
      templateUrl: '/views/directives/ps_pw_toggle.html',
      transclude: true,
      scope: {},
      link: function postLink(scope, element) {

        var input = element.find('input');
        input.addClass('form-control');

        scope.$watch('showPw', function () {
          input.prop('type', scope.showPw ? 'text' : 'password');
        });
        scope.showPw = false;

      }
    };
  });
