'use strict';

/**
* @ngdoc directive
* @name participationApp.directive:formAutofillFix
* @description
* # formAutofillFix
*/
angular.module('ps.directives')
.directive('psCommentView', function (message, api, _) {
  return {
    templateUrl: '/views/directives/ps_comment_view.html',
    restrict: 'E',
    scope: {
      url : '@',
      typed : '@',
      fetch : '=',
      collapsed : '=',
      collapsable : '=',
      readOnly : '=?',
    },

    link: function(scope) {
      var pendingComment = false;
      scope.state = 'initial';
      scope.comments = [];

      scope.newComment = {
        content: '',
        solutionCommentType: null,
      };

      scope.typeFilters = [
        {
          name: 'Alle',
          value: function() {return true;},
        },
        {
          name: 'Info',
          value: 'INFO',
        },
        {
          name: 'Pro',
          value: 'PRO',
        },
        {
          name: 'Contra',
          value: 'CONTRA',
        },
      ];

      scope.types = ['INFO', 'PRO', 'CONTRA'];

      scope.currentTypeFilter = scope.typeFilters[0];

      scope.setFilter = function(filter) {
        scope.currentTypeFilter = filter;
      };

      scope.countComments = function (comments) {
        return _.reduce(comments, function(total, comment) {
          return total + scope.countComments(comment.responses);
        }, comments.length);
      };

      scope.fetchComments = function () {
        if (scope.state !== 'loading') {
          scope.state = 'loading';
          api.get(scope.url, function (comments) {
            scope.comments = comments;
            scope.state = 'done';
          });
        }
      };

      scope.iconForType = function(type) {
        return {
          'PRO': 'fa-fw fa-thumbs-up',
          'CONTRA': 'fa-fw fa-thumbs-down',
          'INFO': 'fa-fw fa-info',
        }[type];
      };

      scope.canPostComment = function () {
        return !scope.typed || !!scope.newComment.solutionCommentType;
      };

      scope.postNewComment = function() {
        if (!pendingComment) {
          var breakPost = false;
          if (scope.newComment.content === '') {
            message.add('comment_empty');
            breakPost = true;
          }
          var data;
          if (scope.typed) {
            if (scope.newComment.solutionCommentType === null) {
              message.add('comment_no_type');
              breakPost = true;
            } else {
              data = scope.newComment;
            }
          } else {
            data = {content: scope.newComment.content};
          }
          if (breakPost) {
            return;
          }
          pendingComment = true;
          api.post(scope.url, data, function (payload) {
            scope.comments[scope.comments.length] = payload;
            scope.newComment.content = '';
            scope.newComment.solutionCommentType = null;
            pendingComment = false;
          });
        }
      };

      scope.$watch('fetch', function(newValue) {
        if (newValue) {
          scope.fetchComments();
          scope.fetch = false;
        }
      });
    },
  };
});
