'use strict';

/**
 * @ngdoc directive
 * @name participationApp.directive:psDoubleClick
 * @description
 * # psDoubleClick
 */
angular.module('ps.directives')
  .directive('psDoubleClick', function ($timeout) {
    return {
      restrict: 'A',
      scope: {
        psDoubleClick: '&',
      },
      link: function postLink(scope, element) {
        var armed = false;

        element.on('click', function (event) {
          if (armed) {
            scope.psDoubleClick({event: event});
          } else {
            armed = true;
            var originalText = element.text();
            element.text('Nochmal klicken zum Bestätigen');

            $timeout(function () {
              armed = false;
              element.text(originalText);
            }, 1000);
          }
        });
      }
    };
  });
