'use strict';

/**
 * @ngdoc directive
 * @name participationApp.directive:filterCtrl
 * @description
 * # filterCtrl
 */
angular.module('ps.directives')
  .directive('psFilterWidget', function ($filter, $location, _) {
    return {
      restrict: 'E',
      templateUrl: '/views/directives/ps_filter_widget.html',

      scope: {
        input: '=',
        output: '=',
        presets: '=',
        activePreset: '=?',
      },

      link: function (scope) {
        scope.query = '';

        scope.clearQuery = function () {
          scope.query = '';
        };

        scope.activateByIndex = function (index) {
          scope.activePreset = scope.presets[index];
        };

        scope.activateById = function (id) {
          scope.activePreset = _.find(scope.presets, 'id', id);
        };

        var update = function () {
          var preset = scope.activePreset;
          $location.search('filter', preset.id);
          $location.search('query', scope.query || null);
          var presetFiltered = $filter('filter')(scope.input, preset.expression);
          scope.output = $filter('filter')(presetFiltered, scope.query);
        };

        if (!scope.activePreset) {
          if ($location.search().filter) {
            scope.activateById($location.search().filter);
          } else {
            scope.activateByIndex(0);
          }
        }
        scope.$watch('query', update);
        scope.$watch('activePreset', update);
        scope.$watch('input', update, true);

        scope.$on('$routeUpdate', function(){
          scope.activateById($location.search().filter);
          scope.query = $location.search().query || '';
        });
      }
    };
  });
