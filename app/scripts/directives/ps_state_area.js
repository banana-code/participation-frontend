'use strict';

/**
* @ngdoc directive
* @name participationApp.directive:formAutofillFix
* @description
* # formAutofillFix
*/
angular.module('ps.directives')
.directive('psStateArea', function () {
  return {
    templateUrl: '/views/directives/ps_state_area.html',
    restrict: 'E',
    transclude: true,
    scope: {
      state : '=',
      message : '=',
    },
    link: function(scope) {
      scope.showContent = false;
      scope.showAnimation = false;
      var showContentOn = [
        'ok',
        'success',
        'finished',
        'ready',
        'done',
        'detected',
        'userShot',
        'calculated',
        'userInput',
        'locked',
      ];
      var showAnimationOn = [
        'searching',
        'loading',
        'pending',
        'detecting',
        'fetching',
        'noImage',
      ];

      scope.$watch('state', function(newValue) {
        scope.showContent = showContentOn.indexOf(newValue) >= 0;
        scope.showAnimation = showAnimationOn.indexOf(newValue) >= 0;
      });
    },
  };
});
