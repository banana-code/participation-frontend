'use strict';

/**
 * @ngdoc directive
 * @name participationApp.directive:badge
 * @description
 * # badge
 */
angular.module('ps.directives')
  .directive('psBadge', function () {
    return {
      templateUrl: '/views/directives/ps_badge.html',
      restrict: 'E',
      scope: {
        icon: '@',
        color: '@',
        size: '@',
      }
    };
  });
