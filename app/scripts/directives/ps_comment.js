'use strict';

/**
* @ngdoc directive
* @name participationApp.directive:formAutofillFix
* @description
* # formAutofillFix
*/
angular.module('ps.directives')
.directive('psComment', function (message, api, RecursionHelper) {
  return {
    templateUrl: '/views/directives/ps_comment.html',
    restrict: 'E',
    scope: {
      comment : '=',
      readOnly : '=?',
    },
    compile: function(element) {
      var link = function(scope) {
        var pendingVote = false;
        var pendingReply = scope.pendingReply = false;
        var comment = scope.comment;
        var commentUrl = '/comments/' + comment.id;
        var adjustVotes = function (type, value) {
          var mapping = {
            'DOWN': 'downVotes',
            'UP': 'upVotes',
          };
          if (mapping.hasOwnProperty(type)) {
            comment[mapping[type]] += value;
          }
        };

        scope.vote = function (type) {
          if (!pendingVote) {
            pendingVote = true;
            var data = {voteType: type};
            if (comment.userVote === type) {
              data.voteType = 'NEUTRAL';
            }
            api.post(commentUrl + '/votes', data, function () {
              adjustVotes(comment.userVote, -1);
              comment.userVote = data.voteType;
              adjustVotes(comment.userVote, +1);
              pendingVote = false;
            });
          }
        };

        scope.iconForType = function(type) {
          return {
            'PRO': 'fa-thumbs-up',
            'CONTRA': 'fa-thumbs-down',
            'INFO': 'fa-info',
          }[type];
        };

        scope.toggleReply = function() {
          comment.showReplyInput = !comment.showReplyInput;
        };

        scope.postReply = function(text) {
          if (!pendingReply) {
            if (text === '' || text === undefined) {
              message.add('comment_empty');
              return;
            }
            pendingReply = true;
            var data = {content: text};
            api.post(commentUrl, data, function (payload) {
              comment.responses[comment.responses.length] = payload;
              comment.showReplyInput = false;
              pendingReply = false;
            });
          }
        };
      };
      return RecursionHelper.compile(element, link);
    }
  };
});
