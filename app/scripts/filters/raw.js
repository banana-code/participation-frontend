'use strict';

/**
 * @ngdoc filter
 * @name participationApp.filter:raw
 * @function
 * @description
 * # raw
 * Filter in the participationApp.
 */
angular.module('ps.filters')
  .filter('raw', function ($sce) {

    return function (input) {
      return $sce.trustAsHtml(input);
    };

  });
