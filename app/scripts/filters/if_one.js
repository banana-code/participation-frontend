'use strict';

/**
 * @ngdoc filter
 * @name participationApp.filter:ifOne
 * @function
 * @description
 * # ifOne
 * Filter in the participationApp.
 */
angular.module('ps.filters')
  .filter('ifOne', function ($filter) {
    return function (input, singular, plural) {
      return $filter('count')(input) === 1 ? singular : plural;
    };
  });
