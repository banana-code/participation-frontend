'use strict';

/**
* @ngdoc filter
* @name participationApp.filter:caps
* @function
* @description
* # caps
* Filter in the participationApp.
*/
angular.module('ps.filters')
.filter('caps', function () {
  return function (input) {
    return (typeof input === 'string') ? input.toUpperCase() : input;
  };
});
