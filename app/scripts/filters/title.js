'use strict';

/**
 * @ngdoc filter
 * @name participationApp.filter:title
 * @function
 * @description
 * # title
 * Filter in the participationApp.
 */
angular.module('ps.filters')
  .filter('title', function () {
    return function (input) {
      return (typeof input !== 'string') ? input : input.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    };
  });
