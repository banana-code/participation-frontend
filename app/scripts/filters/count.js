'use strict';

/**
 * @ngdoc filter
 * @name participationApp.filter:count
 * @function
 * @description
 * # count
 * Filter in the participationApp.
 */
angular.module('ps.filters')
  .filter('count', function () {
    var count = function(input) {
      if (!input) {
        return 0;
      }
      return angular.isNumber(input) ? input : input.length;
    };
    return function (input, empty) {
      empty = angular.isDefined(empty) ? empty : 0;
      return count(input) || empty;
    };
  });
