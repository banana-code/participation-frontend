'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('ActivationCtrl', function ($scope, $routeParams, $location, api, message, crumble) {
    crumble.update();
    $scope.key = '';

    $scope.pending = false;
    $scope.tryActivate = function() {
      if (!$scope.pending) {
        $scope.pending = true;
        api.confirm($scope.key, function (data) {
          message.add(data);
          $scope.pending = false;
          $location.path('/groups');
        }, function (data) {
          message.add(data);
          $scope.pending = false;
        });
      }
    };
    if ($routeParams.key) {
      $scope.key = $routeParams.key;
      $scope.tryActivate();
    }
  });
