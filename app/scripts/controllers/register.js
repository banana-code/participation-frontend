'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('RegisterCtrl', function ($scope, $location, api, message, util, crumble) {

    crumble.update();

    $scope.regForm = {};

    $scope.submit = function (email, alias, password) {
      var data = {
        password: password,
        email: email,
        alias: alias
      };
      api.post('/register', data, function () {
        message.add('register_success');
        $location.path('/activation');
      });
    };
    util.setFocus('registerEmail');
  });
