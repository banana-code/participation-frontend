'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:IssuesViewCtrl
 * @description
 * # IssuesViewCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('IssuesViewCtrl',
    function ($scope, $timeout, $location, $modal, group, issue, message, crumble) {

    $scope.me = group.userMembership;
    $scope.group = group;
    $scope.issue = issue;
    $scope.apiPathIssue = '/groups/' + group.id + '/issues/' + issue.id;

    crumble.update({
      issue: issue,
      group: group,
    });

    $scope.groupParticitage = function () {
      var quota = issue.parcitipatedUserCount / group.memberCount;
      return Math.round(quota * 100);
    };

    $scope.openDeleteIssueDialog = function () {
      $modal.open({
        controller: 'DeleteDialogCtrl',
        templateUrl: '/views/issues/delete_dialog.html',
        resolve: {
          type: function () {return 'Thema';},
          item: function () {return issue;},
        },
      }).result.then(function() {
        deleteIssue();
      });
    };

    var deleteIssue = function () {
      issue.$delete(function () {
        // Delay redirect to account for dialog hide animation
        $timeout(function () {
          message.add('issue_deleted');
          $location.path('/group/' + group.id);
        }, 1000);
      });
    };
  });
