'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:IssuesViewCtrl
 * @description
 * # IssuesViewCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('DeleteDialogCtrl', function ($scope, item, type) {
    $scope.item = item;
    $scope.type = type;
  });
