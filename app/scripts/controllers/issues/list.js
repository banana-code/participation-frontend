'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:IssuesListCtrl
 * @description
 * # IssuesListCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  // Important Notice: This controller's scope is a child of GroupsViewCtrl's scope
  .controller('IssuesListCtrl', function ($scope, Issue, session) {

    $scope.filterPresets = [{
      id: 'all',
      name: 'Alle',
      expression: function () { return true; },
    }, {
      id: 'active',
      name: 'Aktive',
      expression: function (issue) {
        return ['OPEN', 'VOTE_ONLY'].some(function (activeState) {
          return issue.state === activeState;
        });
      }
    }, {
      id: 'closed',
      name: 'Abgeschlossene',
      expression: {'state': 'closed'},
    }, {
      id: 'my',
      name: 'Eigene',
      expression: function (issue) {
        return issue.createdBy.id === session.user().id;
      }
    }, ];

    $scope.issues = Issue.query({
      groupId: $scope.group.id
    });
  });
