'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:IssueEditCtrl
 * @description
 * # IssueEditCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('IssuesEditCtrl',
  function ($scope, $location, group, issue, message, crumble, psMarkdownPreview) {

    var groupPath = '/group/' + group.id;
    function issuePath() {
      return groupPath + '/issue/' + issue.id;
    }

    $scope.group = group;
    $scope.issue = issue;
    $scope.isNew = !issue.id;

    crumble.update({
      group: group,
      issue: issue,
    });

    function isValidDate(date, forPublishing) {
      return (!forPublishing && !date) || !isNaN(date.getTime());
    }

    var validate = function (forPublishing) {
      if (!issue.title) {
        message.add('issue_title_invalid');
        return false;
      }
      if (!isValidDate(issue.solutionEndDate, forPublishing)) {
        message.add('issue_solution_date_invalid');
        return false;
      }
      if (!isValidDate(issue.voteEndDate, forPublishing)) {
        message.add('issue_vote_date_invalid');
        return false;
      }
      return true;
    };

    var ctrlConfigs = {
      create: {
        saveMethod: '$save',
        abortMsg: 'issue_creation_aborted',
        abortPath: groupPath,
        init: function () {
          issue.description = '\n\n#### Übergeordnete Fragestellung\n' +
                              '\n\n#### Bisherige Lösung\n\n';
        }
      },
      edit: {
        saveMethod: '$patch',
        abortMsg: 'issue_edit_aborted',
        abortPath: issuePath(),
        init: function () {
          // Abort editing if issue has already started
          if (new Date() < issue.startDate) {
            message.add('issue_edit_started');
            $location.path(issuePath());
          }
        }
      },
    };
    var ctrlConfig = ctrlConfigs[$scope.isNew ? 'create' : 'edit'];

    function getSaveMethod(forPublishing) {
      var startDate = forPublishing ? 'now' : null;
      var successMsg = forPublishing ? 'issue_publication_success' : 'issue_save_success';
      return function() {
        if (validate(forPublishing)) {
          issue.startDate = startDate;
          issue[ctrlConfig.saveMethod](function () {
            message.add(successMsg);
            $location.path(issuePath());
          });
        }
      };
    }
    $scope.publish = getSaveMethod(true);
    $scope.save = getSaveMethod(false);

    $scope.abort = function () {
      message.add(ctrlConfig.abortMsg);
      $location.path(ctrlConfig.abortPath);
    };

    $scope.showMarkdownPreview = function () {
      psMarkdownPreview(issue.description);
    };

    ctrlConfig.init();
  });
