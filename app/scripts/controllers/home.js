'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('HomeCtrl', function ($scope, session, crumble) {

    $scope.session = session;
    $scope.homeLoggedIn = '/groups?filter=my';

    crumble.update();

  });
