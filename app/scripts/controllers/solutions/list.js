'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:SolutionsListCtrl
 * @description
 * # SolutionsListCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  // Important Notice: This controller's scope is a child of IssuesViewCtrl's scope
  .controller('SolutionsListCtrl', function ($scope, $timeout, $modal, api, message, Solution) {

    $scope.filterPresets = [{
      id: 'all',
      name: 'Alle',
      expression: function () { return true; },
    }, {
      id: 'unrated',
      name: 'Unbewertet',
      expression: function (solution) {
        return solution.userResistance === null;
      },
    }, {
      id: 'rated',
      name: 'Bewertet',
      expression: function (solution) {
        return solution.userResistance !== null;
      },
    }, ];

    $scope.fetchSolutions = function () {
      $scope.loading = true;
      Solution.query({
        groupId: $scope.group.id,
        issueId: $scope.issue.id
      }, function(solutions) {
        $scope.loading = false;
        $scope.solutions = solutions;
      });
    };

    $scope.teaser = function (text) {
      var teaserLength = 140;
      return text.length > teaserLength ? text.substr(0, teaserLength) + '…' : text;
    };

    $scope.resistage = function () {
      return Math.round(this.solution.resistance * 10);
    };

    $scope.acceptage = function () {
      return 100 - this.resistage();
    };

    $scope.solutionParticitage = function () {
      var quota = this.solution.voteCount / $scope.issue.parcitipatedUserCount;
      return Math.round(quota * 100);
    };

    $scope.toggleSolution = function (solution) {
        solution.opened = !solution.opened;
        if (solution.opened) {
          solution.fetchComments = true;
        }
    };

    $scope.updateResistance = function (solution) {
      // Since we are notified of every value change, we debounce stuff a bit
      $timeout.cancel(solution.pendingChange);
      solution.pendingChange = $timeout(function () {
        postResistance(solution);
      }, 400);
    };

    $scope.openDeleteSolutionDialog = function (solution) {
      $modal.open({
        controller: 'DeleteDialogCtrl',
        templateUrl: '/views/issues/delete_dialog.html',
        resolve: {
          type: function () {return 'Lösungsvorschlag';},
          item: function () {return solution;},
        },
      }).result.then(function() {
        deleteSolution(solution);
      });
    };

    var postResistance = function (solution) {
      var url = $scope.apiPathIssue + '/solutions/' + solution.id + '/resistances';
      var data = {
        rating: solution.userResistance,
      };
      api.post(url, data);
    };

    var deleteSolution = function (solution) {
      solution.$delete(function () {
        // delay the removing until the dialog close animation is finished
        $timeout(function () {
          message.add('solution_deleted');
          // remove deleted solution from list
          var idx = $scope.solutions.indexOf(solution);
          $scope.solutions.splice(idx, 1);
        }, 1000);
      });
    };

    $scope.fetchSolutions();
  });
