'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:SolutionsNewCtrl
 * @description
 * # SolutionsNewCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('SolutionsNewCtrl', function ($scope, $location, issue, Solution, message, crumble, psMarkdownPreview) {

    $scope.issue = issue;
    $scope.solution = new Solution({
      groupId: issue.groupId,
      issueId: issue.id,
    });
    crumble.update();

    function backToIssue (msg) {
      message.add(msg);
      $location.path('/group/' + issue.groupId + '/issue/' + issue.id);
    }

    $scope.submit = function () {
      $scope.solution.$save(function () {
        backToIssue('solution_creation_success');
      });
    };

    $scope.abort = function () {
      backToIssue('solution_creation_aborted');
    };

    $scope.showMarkdownPreview = function () {
      psMarkdownPreview($scope.solution.description);
    };
  });
