'use strict';

angular.module('participationApp')
  .controller('MembersCtrl', function ($scope, api, session, crumble, psConfirm) {

    crumble.update();

    $scope.prefix = '';
    $scope.fetchedAccounts = [];

    $scope.getAccounts = function () {
      var data = { aliasPrefix: $scope.prefix };
      return api.get('/admin/accounts', function (accounts) {
        $scope.fetchedAccounts = accounts;
      }, angular.noop, data);
    };

    // This line takes care that the list of users is not empty once you open
    // this members view. As long as we don't have pagination, this will query
    // the server for all users of the system. That might be a lot!
    $scope.getAccounts();

    $scope.getUtils = function (account) {
      var utils = (function () {
        function changeState(account, state, msg) {
          psConfirm(msg.replace('{alias}', account.user.alias))
            .then(function() {
              var data = { accountState: state };
              api.patch('/admin/accounts/' + account.id, data, function() {
                account.state = state;
              });
            });
        }
        function lock(account) {
          changeState(account, 'LOCKED', 'Willst du "{alias}" wirklich sperren?');
        }
        function unlock(account) {
          changeState(account, 'VALID', 'Willst du "{alias}" wirklich entsperren?');
        }
        function makeAdmin(account) {
          changeState(account, 'ADMIN', 'Willst du "{alias}" wirklich Admin-Rechte geben?');
        }
        function removeAdmin(account) {
          changeState(account, 'VALID', 'Willst du "{alias}" wirklich die Admin-Rechte entziehen?');
        }

        var titleLock = 'Diesen Benutzer sperren';
        var titleUnlock = 'Diesen Benutzer entsperren';
        var titleMakeAdmin = 'Diesem Benutzer Admin-Rechte geben';
        var titleRemoveAdmin = 'Diesem Benutzer die Admin-Rechte entziehen';
        var iconLock = 'fa fa-lock fa-fw';
        var iconUnlock = 'fa fa-unlock fa-fw';
        var iconAdmin = 'fa fa-level-up fa-fw';
        var iconUnadmin = 'fa fa-level-down fa-fw';

        return {
          SELF: {
            state: {
              icon: 'fa fa-font',
              title: 'Das bist du',
              color: 'primary',
            },
            lock: {
              title: titleLock,
              icon: iconLock,
              color: 'default disabled',
              fcn: angular.noop,
            },
            admin: {
              title: titleMakeAdmin,
              icon: iconAdmin,
              color: 'default disabled',
              fcn: angular.noop,
            }
          },
          VALID: {
            state: {
              icon: 'fa fa-user',
              title: 'Dieser Account ist gültig',
              color: 'success',
            },
            lock: {
              title: titleLock,
              icon: iconLock,
              color: 'warning',
              fcn: lock,
            },
            admin: {
              title: titleMakeAdmin,
              icon: iconAdmin,
              color: 'danger',
              fcn: makeAdmin,
            },
          },
          UNCONFIRMED: {
            state: {
              icon: 'fa fa-clock-o',
              title: 'Dieser Account wurde noch nicht verifiziert',
              color: 'info',
            },
            lock: {
              title: titleLock,
              icon: iconLock,
              color: 'default disabled',
              fcn: angular.noop,
            },
            admin: {
              title: titleMakeAdmin,
              icon: iconAdmin,
              color: 'default disabled',
              fcn: angular.noop,
            },
          },
          LOCKED: {
            state: {
              icon: 'fa fa-lock',
              title: 'Dieser Account ist gesperrt',
              color: 'danger',
            },
            lock: {
              title: titleUnlock,
              icon: iconUnlock,
              color: 'success',
              fcn: unlock,
            },
            admin: {
              title: titleMakeAdmin,
              icon: iconAdmin,
              color: 'danger',
              fcn: makeAdmin,
            },
          },
          ADMIN: {
            state: {
              icon: 'fa fa-font',
              title: 'Dieser Account ist ein Admin-Account',
              color: 'primary',
            },
            lock: {
              title: titleLock,
              icon: iconLock,
              color: 'warning',
              fcn: lock,
            },
            admin: {
              title: titleRemoveAdmin,
              icon: iconUnadmin,
              color: 'warning',
              fcn: removeAdmin,
            },
          }
        };
      })();

      var isSelf = account.user.id === session.user().id;
      return utils[isSelf ? 'SELF' : account.state];
    };

  });
