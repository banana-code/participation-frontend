'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('ForgotPasswordCtrl', function ($scope, $location, api, message, crumble) {
    $scope.pending = false;
    crumble.update();
    $scope.tryRequest = function(email) {
      if (!$scope.pending) {
        $scope.pending = true;
        api.post('/resetPassword', {email:email}, function() {
          $location.path('/resetPassword');
          $scope.pending = false;
        }, function (data) {
          message.add(data);
          $scope.pending = false;
        });
      }
    };
  });
