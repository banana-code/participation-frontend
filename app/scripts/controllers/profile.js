'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('ProfileCtrl', function ($scope, session, api, message, crumble) {
    $scope.session = session;
    $scope.passwords = {};
    $scope.alias = session.user().alias;

    crumble.update();

    $scope.saveAlias = function () {
      if ($scope.alias === session.user().alias) {
        return;
      }
      var user = {
        alias: $scope.alias
      };
      api.patch('/me/alias', user, function () {
        session.user().alias = $scope.alias;
        message.add('alias_change_success');
      });
    };

    $scope.savePassword = function () {
      var account = {
        oldPassword: $scope.passwords.old,
        newPassword: $scope.passwords.new,
      };
      api.patch('/me/password', account, function () {
        message.add('password_change_success');
      });
    };
  });
