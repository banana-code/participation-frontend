'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('HeaderCtrl', function ($scope, $location, $log, config, api, message, session, util, messages, crumble) {

    $scope.crumble = crumble;
    $scope.session = session;
    $scope.loginForm = {};
    $scope.nav = {isCollapsed: false};

    // Show a button to trigger random messages
    if (config.debug) {
      var msgs = Object.keys(messages);
      $scope.testMessage = function () {
        var msg = msgs[Math.floor(Math.random() * (msgs.length - 1))];
        $log.log('Triggered message with id ' + msg);
        message.add(msg);
      };
    }

    $scope.$on('$routeChangeStart', function() {
      $scope.nav.isCollapsed = true;
    });

    $scope.navTemplate = function () {
      return '/views/partials/nav_' + (session.loggedIn() ? 'user' : 'guest') + '.html';
    };

    $scope.onToggleLogin = function (open) {
      if (open) {
        util.setFocus('loginEmail');
      }
    };

    $scope.preventClose = function (e) {
      e.stopPropagation();
    };

    $scope.login = function () {
      api.login($scope.loginForm.email, $scope.loginForm.password, function () {
        $location.path('/groups').search('filter', 'my');
        $scope.loginForm.email = '';
        $scope.loginForm.password = '';
      });
    };

    $scope.logout = function () {
      api.logout(function () {
        message.add('logout_success');
        if ($location.path() !== '/') {
          $location.path('/');
        }
      });
    };

  });
