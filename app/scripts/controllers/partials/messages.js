'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:MessageCtrl
 * @description
 * # MessageCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('MessageCtrl', function ($scope, message) {
    $scope.message = message;
  });
