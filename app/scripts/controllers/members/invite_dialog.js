'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:MemberinvitectrlCtrl
 * @description
 * # MemberinvitectrlCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('MembersInviteDialogCtrl',
  function ($scope, $modalInstance, _, group, members, api, message, util) {

    $modalInstance.opened.then(function () {
      util.setFocus('invitationInput');
    });

    $scope.findUsersByAlias = function (prefix) {
      return api.get('/users', _.noop, _.noop, { aliasPrefix: prefix })
        .then(_.property('data'));
    };

    $scope.invite = function () {
      var user = $scope.invite.user;
      var member = _.find(members, function (m) {
        return m.user && (m.user.id === user.id);
      });

      if (member) {
        if (member.isRequested()) {
          member.state = 'MEMBER';
          member.$patch();
          message.add('invite_user_requested');
        } else if (member.isInvited()) {
          message.add('invite_already_invited');
        } else {
          message.add('invite_already_member');
        }
        return;
      }

      var newMember = group.getBlankMembership(user).$save(function (member) {
        members.push(member);
        message.add('invite_success');
      });

      $modalInstance.close(newMember);
    };

  });
