'use strict';

angular.module('ps.controllers')
  .controller('MembersEditDialogCtrl',
  function ($scope, me, selectedMember, membershipTools) {

    $scope.mt = membershipTools;

    $scope.me = me;
    $scope.selectedMember = selectedMember;

    var isMe = $scope.isMe = function () {
      return selectedMember.user.id === me.user.id;
    };

    $scope.showStateChange = function () {
      return !isMe() && me.isOwner();
    };

    $scope.showResignButton = function () {
      return isMe() && me.atLeastModerator();
    };

    $scope.saveState = function () {
      selectedMember.$patch();
    };

  });
