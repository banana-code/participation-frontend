'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:MembersListCtrl
 * @description
 * # MembersListCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('MembersListCtrl', function ($scope, $location, $modal, _, group, message, crumble) {

    crumble.update({group: group});

    var presets = {
      member: [{
        id: 'members',
        name: 'Mitglieder',
        expression: function(member) {
          return member.atLeastMember();
        },
      }, {
        id: 'owners',
        name: 'Eigner',
        expression: {'state': 'OWNER'},
      }, {
        id: 'mods',
        name: 'Moderatoren',
        expression: {'state': 'MODERATOR'},
      }, ],
    };
    presets.mod = presets.member.concat([{
      id: 'invites',
      name: 'Eingeladen',
      expression: {'state': 'INVITED'},
    }, {
      id: 'requests',
      name: 'Anfragen',
      expression: {'state': 'REQUESTED'},
    }, ]);

    $scope.group = group;

    var me = $scope.me = function () {
      return group.userMembership;
    };

    $scope.meMod = function () {
      return me().atLeastModerator();
    };

    $scope.isMe = function (member) {
      return member.user.id === me().user.id;
    };

    $scope.meCanEdit = function (member) {
      if (!member.atLeastMember()) {
        return false;
      }
      return $scope.isMe(member) ||
        me().isOwner() ||
        ($scope.meMod() && member.isMember());
    };

    $scope.filterPresets = function () {
      return $scope.meMod() ? presets.mod : presets.member;
    };

    $scope.members = group.getMembers(function (members) {
      // Ensure reference equality for own membership
      if (!me().isUnknown()) {
        group.userMembership = _.find(members, 'id', me().id);
      }
    });

    $scope.edit = function (member) {
      $modal.open({
        controller: 'MembersEditDialogCtrl',
        templateUrl: '/views/members/edit_dialog.html',
        resolve: {
          me: me,
          selectedMember: _.constant(member),
        }
      })
      .result.then(function (action) {
        ({ // Coding Ninja style
          downgrade: downgradeMember,
          delete: $scope.delete,
        })[action](member);
      });
    };

    var downgradeMember = function (member) {
      var targetState = member.isOwner() ? 'MODERATOR' : 'MEMBER';
      $scope.updateState(member, targetState);
    };

    $scope.updateState = function (member, state) {
      member.state = state;
      member.$patch();
    };

    $scope.saveState = function (member) {
      member.$patch();
    };

    $scope.delete = function (member) {
      member.$delete(function () {
        _.pull($scope.members, member);

        if ($scope.isMe(member)) {
          message.add('group_left');
          $location.path('/');
        }
      });
    };

    $scope.showInviteDialog = function() {
      $modal.open({
        controller: 'MembersInviteDialogCtrl',
        templateUrl: '/views/members/invite_dialog.html',
        resolve: {
          group: _.constant(group),
          members: _.constant($scope.members),
        }
      });
    };

  });
