'use strict';

angular
  .module('ps.controllers', [
    'ps.directives',
    'ui.bootstrap',
  ]);
