'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:HelpCtrl
 * @description
 * # HelpCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('HelpCtrl', function ($scope, $routeParams, crumble) {
    if (typeof $routeParams.topic === 'undefined') {
      $scope.topic = 'index';
      crumble.update();
    } else {
      $scope.topic = $routeParams.topic;
      crumble.update({topic: $scope.topic});
    }
  });
