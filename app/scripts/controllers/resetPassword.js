'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('ResetPasswordCtrl', function ($scope, $routeParams, $location, api, message, crumble) {
    crumble.update();
    $scope.data = {
      key: '',
      newPassword: '',
    };

    $scope.pending = false;
    $scope.tryReset = function() {
      if (!$scope.pending) {
        $scope.pending = true;
        api.post('/setNewPassword', $scope.data, function (data) {
          api.setSessionFromResponse(data);
          message.add('password_change_success');
          $scope.pending = false;
          $location.path('/groups');
        }, function (data) {
          message.add(data);
          $scope.pending = false;
        });
      }
    };

    if ($routeParams.key) {
      $scope.data.key = $routeParams.key;
    }
  });
