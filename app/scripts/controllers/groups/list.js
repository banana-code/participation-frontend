'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:GroupsListCtrl
 * @description
 * # GroupsListCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('GroupsListCtrl', function ($scope, _, groups, crumble) {

    crumble.update();
    $scope.groups = groups;

    function getMembershipFilter(predicateName) {
      return function(group) {
        return group.userMembership[predicateName]();
      };
    }

    $scope.filterPresets = [{
      id: 'all',
      name: 'Alle',
      expression: _.constant(true),
    }, {
      id: 'my',
      name: 'Meine',
      expression: getMembershipFilter('atLeastMember'),
    }, {
      id: 'invited',
      name: 'Eingeladen',
      expression: getMembershipFilter('isInvited'),
    }, {
      id: 'requested',
      name: 'Angefragt',
      expression: getMembershipFilter('isRequested'),
    }, ];
  });
