'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:GroupsEditCtrl
 * @description
 * # GroupsEditCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('GroupsEditCtrl', function ($scope, $location, group, message, crumble) {

    crumble.update({group: group});

    $scope.securityPhrase = 'Ja ich will diese Gruppe löschen';
    $scope.group = group;
    $scope.isNew = !group.id;

    var settings;
    if ($scope.isNew) {
      settings = {
        saveMethod: '$save',
        saveMsg: 'group_creation_success',
        abortMsg: 'group_creation_aborted',
        abortPath: '/groups',
      };
    } else {
      settings = {
        saveMethod: '$patch',
        saveMsg: 'group_save_success',
        abortMsg: 'group_edit_aborted',
        abortPath: '/group/' + group.id,
      };
    }

    var validate = function () {
      var ret = true;
      // title
      if (!group.title) {
        message.add('group_title_invalid');
        ret = false;
      }
      return ret;
    };

    $scope.save = function () {
      if (validate()) {
        group[settings.saveMethod](function () {
          message.add(settings.saveMsg);
          $location.path('/group/' + group.id);
        });
      }
    };

    $scope.delete = function () {
      group.$delete(function () {
        message.add('group_deleted');
        $location.path('/groups');
      });
    };

    $scope.abort = function () {
      message.add(settings.abortMsg);
      $location.path(settings.abortPath);
    };

  });
