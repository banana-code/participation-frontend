'use strict';

/**
 * @ngdoc function
 * @name participationApp.controller:GroupsViewCtrl
 * @description
 * # GroupsViewCtrl
 * Controller of the participationApp
 */
angular.module('ps.controllers')
  .controller('GroupsViewCtrl', function ($scope, $location, _, group, Member, session, message, crumble, psConfirm) {

    crumble.update({group: group});
    $scope.session = session;

    $scope.group = group;
    $scope.session = session;
    $scope.me = group.userMembership;

    $scope.requestMembership = function () {
      $scope.me.$save();
    };

    $scope.acceptInvite = function () {
      // We can't use the instance here, since we want the view to change only
      // after the request completes successfully
      Member.patch(_.defaults({state: 'MEMBER'}, $scope.me), function (member) {
        $scope.me = member;
      });
    };

    $scope.leave = function () {
      $scope.me.$delete(function () {
        message.add('group_leave_success');
        $scope.me = group.getBlankMembership(session.user());
      });
    };

    function deleteGroup() {
      group.$delete(function () {
        message.add('group_deleted');
        $location.path('/groups');
      });
    }

    $scope.deleteGroup = function() {
      psConfirm('Bist du sicher, dass du die Gruppe \"' + $scope.group.title + '\" löschen willst?')
        .then(deleteGroup);
    };

  });
