# Participation Frontend
This is a frontend for the Participation Service written in [AngularJS](https://angularjs.org/).

It depends on a running instance of the [Participation Backend](https://gitlab.com/banana-code/participation-backend).


# Configuration
Setup the variables in [`app/scripts/services/config.js`](app/scripts/services/config.js):

* `apiUrl` defines the location of your backend
* `debug` triggers debug output around the app

# Development
Learn how to contribute.

## Make your machine ready
To develop you need the following installed on your system:
* [Node.js](https://nodejs.org/en/) with [npm](https://github.com/npm/npm)
* [ruby](https://www.ruby-lang.org/en/documentation/installation/)
* [git](https://git-scm.com/)

After installing npm you should setup your commandline tools globally:
```
npm install -g grunt-cli bower
```

Also you'll need [compass](http://compass-style.org/):
```
gem install compass
```

Now you can install the modules for this project. Run this line in the project directory:
```
npm install && bower install
```

There you go.

## The build system
We use [grunt](http://gruntjs.com/) as our build tool. Here are the most important tasks we have defined. For more available tasks see [`Gruntfile.js`](Gruntfile.js).

* `grunt` builds the application and runs tests and linters
* `grunt serve` start development server with live reload etc.
* `grunt serve:dist` starts server in dist folder, so the built app can be checked
* `grunt build` build the application
* `grunt lint` run linters
* `grunt test` run tests

You can switch the build target by using the `--target=<TARGET_NAME>` option. This will use `app/scripts/services/config.<TARGET_NAME>.js` as the configuration file for the build. `<TARGET_NAME>` defaults to `development`.


## Troubleshooting

#### stack overflow during `grunt watch` (unix)
If you develop on a Unix machine it can happen easily that `grunt watch` (which is used by `grunt serve`) [exceeds the maximum watches](http://stackoverflow.com/questions/16748737/grunt-watch-error-waiting-fatal-error-watch-enospc) on files for your system.
You can fix this by increasing the number of watches:

```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
# License

*Copyright 2015 Felix Hofmann, Leonard Otto, Maximilian Schick, Daniel Stroh and
Raphael von der Grün*

The words *this program* refer to this repository in its entirety. Unless
explicitly stated otherwise, the following license conditions apply to any files
that are included in this repository.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Afffero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Afffero General Public License for more
details.

You should have received a copy of the GNU Afffero General Public License along
with this program. If not, see <http://www.gnu.org/licenses/>.
