# TODOs

## Refactoring

### Ludicrous Priority
* **Testing**

### High Priority
* slider thumb in IE und FF hässlich

### Medium Priority
* rename membershipTools to `roleTools`
* move `me` to `groupsTools` (generate fns)
* introduce url service (one place to store named frontend urls)
* Konsequente Fehlermeldungen / Redirects bei unauthorized responses

### Low Priority
* slim down util.js (see TODOs there)
* look for unchecked group.userState accesses

### Nice to have
* remove Glyphicon includes?
* convert typeof checks to angular checks (readability)
